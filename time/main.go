package main

import (
	"fmt"
	"time"
)

func main() {
	// testDateTimeString2time()
	// fmt.Println("hello")
	// test2()
	//test3()
	//init the loc
	loc, _ := time.LoadLocation("Europe/London")

	//set timezone,
	now := time.Now().In(loc)

	fmt.Println(time.Now().String())
	fmt.Println(now)
}

type beginDate struct {
	d1 time.Time
}



func testDateTimeString2time() {
	// input := "30/06/2019 22:15:22"
	input := "2019-06-12T00:00:00Z"
	layout := "02/01/2006 15:04:05"

	t, _ := time.Parse(layout, input)
	fmt.Println(t)                               // 2017-08-31 00:00:00 +0000 UTC
	fmt.Println(t.Format("02/01/2006 15:04:05")) // 31-Aug-2017
	// _,err:=fmt.Println(t.Format("02/01/2006")) // 31-Aug-2017
	// fmt.Println("error ",err.Error())

	bd := beginDate{}
	bd.d1 = t
	fmt.Println("bd.d1 = ", bd.d1)

}

func test2() {
	p := fmt.Println
	// t := time.Now()
	t1, _ := time.Parse(
		time.RFC3339,
		"2019-06-12T00:00:00Z")
	p(t1)
	// p(t.Format("3:04PM"))
	// p(t.Format("Mon Jan _2 15:04:05 2006"))
	// p(t.Format("2006-01-02T15:04:05.999999-07:00"))
	// form := "3 04 PM"
	// t2, _ := time.Parse(form, "8 41 PM")
	// p(t2)

	// fmt.Printf("%d-%02d-%02dT%02d:%02d:%02d-00:00\n",
	// t.Year(), t.Month(), t.Day(),
	// t.Hour(), t.Minute(), t.Second())

	// ansic := "Mon Jan _2 15:04:05 2006"
	// _, e = time.Parse(ansic, "8:41PM")
	// p(e)

	// p(t.Format(time.RFC3339))

	bd := beginDate{}
	bd.d1 = t1
	p("time outputt : ", bd.d1)

}

func test3() {
	// layout := "02/01/2006 15:04:05"

	refDocdatetime, _ := time.Parse(time.RFC3339, "2001-01-01T00:00:00Z")
	fmt.Printf("ref doc : %v", refDocdatetime)

	// patern : dd/mm/yyyy hh:mm:ss"
	crdatetime := time.Now()
	fmt.Println("\n create date : ", crdatetime)

}
