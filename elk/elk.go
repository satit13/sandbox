package elk

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

const (
	elkServerURL = "http://34.126.75.184:9210"
	xAccessToken = "f1069095-cf6b-433f-b1ed-6b33cb69ae16"
	elkPostNewDataEndpoint = "/search/v1/document/list"
	elkPostNewMethod = "POST"
	
	elkGetSearchPrefixEndpoint = "/search/v1/prefix"
	


)


type Search struct {
	Keyword string `json:"key_word"`
}

type keyWord struct {
	Query  string       `json:"query"`
	Fields []subKeyWord `json:"fields"`
}

type subKeyWord struct {
	Name string `json:"name"`
	Size int    `json:"size"`
}

type eResponse struct {
	Success bool      `json:"success"`
	Message string    `json:"message"`
	Data    []elkData `json:"data"`
}

type elkData struct {
	Field string `json:"field"`
	Match []res  `json:"match"`
}

type res struct {
	ID     string  `json:"id"`
	Score  float32 `json:"score"`
	Source string  `json:"source"`
}

func makeKeyword(keyword string) keyWord {
	k := keyWord{}
	k.Query = keyword
	sub := subKeyWord{}
	sub.Name = "item_name"
	sub.Size = 5

	k.Fields = append(k.Fields, sub)
	fmt.Println(k)
	return k
}

func GetSearch(keyword string) (err error) {
	url := elkServerURL+elkGetSearchPrefixEndpoint

	b := new(bytes.Buffer)

	// make payload for search 
	c := makeKeyword(keyword)

	json.NewEncoder(b).Encode(c)

	// fmt.Println("payload : ", b)
	fmt.Printf("\n Search Keyword %v \n\n", c)
	client := http.Client{}
	req, err := http.NewRequest(elkPostNewMethod, url, b)
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	req.Header.Add("X-Access-Token", xAccessToken)
	req.Header.Set("Connection", "close")
	fmt.Printf("\n payload : %v \n ", b)
	resp, err := client.Do(req)
	body, _ := ioutil.ReadAll(resp.Body)

	fmt.Printf("	Response Body: %v \n", string(body))

	var p eResponse
	err = json.Unmarshal(body, &p)
	fmt.Printf("success %v \n", p.Success)
	fmt.Println(p)
	fmt.Println("X-Access-Token : ", xAccessToken)
	fmt.Println("	Endpoint : ", url)
	defer req.Body.Close()

	if err != nil {
		log.Println(err)
		return err
	}
	if resp.StatusCode >= 200 && resp.StatusCode <= 204 {
		return nil
	} else {
		return errors.New(string(body))
	}

	//fmt.Println("Post ลูกค้าใหม่ Cloud -> customer.send()")
	return nil
}

func createData() TopLevel {
	// var es Source
	es := Source{}
	es.Code = "41054"
	es.ItemName = "satit nana"
	es.Barcode1 = "41054"
	es.SaleUnit = "คน"
	es.Family = "user"
	es.Department = "user"
	es.Category = "user"
	es.RetailPrice = 10
	es.WholesalePrice = 9

	eps := []Pack{}
	ep := Pack{ID: "41054", Source: es}
	eps = append(eps, ep)

	et := TopLevel{eps}
	return et
}


func Post() error {
	postData := createData()
	url := elkServerURL+ elkPostNewDataEndpoint

	//fmt.Println("method *bcar.send() to Endpoint ", url)
	// Send host uuid with sale post data.
	
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(postData)
	// fmt.Println("payload : ", b)
	fmt.Printf("\n POST Data  %v \n\n", postData)
	client := http.Client{}
	req, err := http.NewRequest(elkPostNewMethod, url, b)
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	req.Header.Add("X-Access-Token", xAccessToken)
	req.Header.Set("Connection", "close")
	fmt.Printf("\n payload : %v \n ", b)
	resp, err := client.Do(req)
	body, _ := ioutil.ReadAll(resp.Body)

	fmt.Printf("	Response Body: %v \n", string(body))

	var p eResponse
	err = json.Unmarshal(body, &p)
	fmt.Printf("success %v \n", p.Success)
	fmt.Println(p)
	fmt.Println("X-Access-Token : ", xAccessToken)
	fmt.Println("	Endpoint : ", url)
	defer req.Body.Close()

	if err != nil {
		log.Println(err)
		return err
	}
	if resp.StatusCode >= 200 && resp.StatusCode <= 204 {
		return nil
	} else {
		return errors.New(string(body))
	}

	//fmt.Println("Post ลูกค้าใหม่ Cloud -> customer.send()")
	return nil

}


func Put() error {
	postData := createData()
	url := elkServerURL+ elkPostNewDataEndpoint

	//fmt.Println("method *bcar.send() to Endpoint ", url)
	// Send host uuid with sale post data.
	
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(postData)
	// fmt.Println("payload : ", b)
	fmt.Printf("\n POST Data  %v \n\n", postData)
	client := http.Client{}
	req, err := http.NewRequest("PUT", url, b)
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	req.Header.Add("X-Access-Token", xAccessToken)
	req.Header.Set("Connection", "close")
	fmt.Printf("\n payload : %v \n ", b)
	resp, err := client.Do(req)
	body, _ := ioutil.ReadAll(resp.Body)

	fmt.Printf("	Response Body: %v \n", string(body))

	var p eResponse
	err = json.Unmarshal(body, &p)
	fmt.Printf("success %v \n", p.Success)
	fmt.Println(p)
	fmt.Println("X-Access-Token : ", xAccessToken)
	fmt.Println("	Endpoint : ", url)
	defer req.Body.Close()

	if err != nil {
		log.Println(err)
		return err
	}
	if resp.StatusCode >= 200 && resp.StatusCode <= 204 {
		return nil
	} else {
		return errors.New(string(body))
	}

	//fmt.Println("Post ลูกค้าใหม่ Cloud -> customer.send()")
	return nil

}

func CreateDelData()DelData{
	d := Del{ID:"41054"}
	ds := []Del{d}
	dd := DelData{ds}
	return dd
}


func Deldata() error {
	postData := CreateDelData()
	url := elkServerURL+ elkPostNewDataEndpoint

	//fmt.Println("method *bcar.send() to Endpoint ", url)
	// Send host uuid with sale post data.
	
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(postData)
	// fmt.Println("payload : ", b)
	fmt.Printf("\n POST Data  %v \n\n", postData)
	client := http.Client{}
	req, err := http.NewRequest("DELETE", url, b)
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	req.Header.Add("X-Access-Token", xAccessToken)
	req.Header.Set("Connection", "close")
	fmt.Printf("\n payload : %v \n ", b)
	resp, err := client.Do(req)
	body, _ := ioutil.ReadAll(resp.Body)

	fmt.Printf("	Response Body: %v \n", string(body))

	var p eResponse
	err = json.Unmarshal(body, &p)
	fmt.Printf("success %v \n", p.Success)
	fmt.Println(p)
	fmt.Println("X-Access-Token : ", xAccessToken)
	fmt.Println("	Endpoint : ", url)
	defer req.Body.Close()

	if err != nil {
		log.Println(err)
		return err
	}
	if resp.StatusCode >= 200 && resp.StatusCode <= 204 {
		return nil
	} else {
		return errors.New(string(body))
	}

	//fmt.Println("Post ลูกค้าใหม่ Cloud -> customer.send()")
	return nil

}

