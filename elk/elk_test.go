package elk_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/satit13/sandbox/elk"
)

func TestElkPostNewData(t *testing.T){
	err := elk.Post()
	assert.Equal(t, err, nil)
}

func TestElkGetSearchKeyword(t *testing.T){
	err := elk.GetSearch("satit")
	assert.Equal(t, err, nil)
}


func TestElkPutUpdateData(t *testing.T){
	err := elk.Put()
	assert.Equal(t, err, nil)
}


func TestElkDelData(t *testing.T){
	err := elk.Deldata()
	assert.Equal(t, err, nil)
}



