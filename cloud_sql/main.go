package main

import (
	"database/sql"
	"fmt"

	// without the underscore _, you will get imported but not
	// used error message
	"crypto/tls"
	"crypto/x509"
	"github.com/go-sql-driver/mysql"
	"io/ioutil"
	"log"
	//"upper.io/db.v3"
)

var mysqls *sql.DB

// var mysqlx *sqlx.DB
var (
	gEnv     = "development" //default
	gSSLMode = "disable"
	bPort    = "3306"
	myHost   = "perfect_db.extensionsoft.biz"
	myUser   = "perfect"
	myDb     = "perfect"
	myPass   = "P@ssw0rd"
)

func main() {

	// conn, err := ConnectDB(myUser, myDb, myHost, myPass, bPort)
	// if err != nil {
	// 	fmt.Println(err.Error())
	// } else {
	// 	fmt.Println("connected ")
	// }
	// defer conn.Close()

	conn, err := connectTLS()
	if err != nil {
		log.Printf("error connect ssl mode %v", err.Error())
		return
	}
	defer conn.Close()
	// var code string
	// sql := `select code  from npdl.Item limit ?`
	// rs := conn.QueryRow(sql, 1)

	fmt.Println("Open Connection success")
	// err = rs.Scan(&code)
	// if err != nil {
	// 	log.Printf("rs.Scan Error %v", err.Error())
	// 	return
	// }
	// fmt.Printf("bank code : %v", code)

}

// ConnectDB : connect normal way db
func ConnectDB(user string, dbName string, host string, pass string, port string) (db *sql.DB, err error) {

	dsn := user + ":" + pass + "@tcp(" + host + ":" + port + ")/" + dbName + "?parseTime=true&charset=utf8&loc=Local"
	db, err = sql.Open("mysql", dsn)
	if err != nil {
		fmt.Println("sql error =", err)
		return nil, err
	}
	log.Println("connect success", dsn)
	db.Exec("use " + dbName)

	return db, nil
}

func connectTLS() (*sql.DB, error) {
	fmt.Println("start TLS ")
	rootCertPool := x509.NewCertPool()
	pem, err := ioutil.ReadFile("./server-ca.pem")
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	fmt.Println("pem readfile ..OK ")
	if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
		log.Fatal("Failed to append PEM.")
		return nil, err
	}
	fmt.Println("rootCertPool  ..OK ")

	clientCert := make([]tls.Certificate, 0, 1)
	certs, err := tls.LoadX509KeyPair("./client-cert.pem", "./client-key.pem")
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	fmt.Println("carts make ..OK ")

	clientCert = append(clientCert, certs)
	mysql.RegisterTLSConfig("custom", &tls.Config{
		RootCAs:      rootCertPool,
		Certificates: clientCert,
	})
	fmt.Println("make clientCert object..ok ")

	//str := "dbname=c9dev user=pgc9 password=pgc9 host=35.247.181.65 sslmode=require port=5432 sslcert=secret/client.crt sslkey=secret/client.key"

	db, err := sql.Open("mysql", "idev@tcp(35.186.144.46:3306)/npdl?tls=custom")
	//db, err := sql.Open("mysql", "nopadol:[ibdkifu88@tcp(35.186.144.46:3306)/npdl?sslmode=require&ssl-cert=client-cert.pem&ssl-key=client-key.pem&ssl-ca=server-ca.pem")

	// mysql -u root -p -h 35.186.144.46 \
	// --ssl-ca=server-ca.pem --ssl-cert=client-cert.pem \
	// --ssl-key=client-key.pem

	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	// fmt.Println("sql.Open ...OK ")
	// x := db.Ping()
	// if x != nil {
	// 	fmt.Println("error db ping ", x.Error())
	// 	return nil, err
	// }
	// // defer db.Close()
	// fmt.Println("connected ssl ")

	var row string
	err = db.QueryRow("SHOW DATABASES").Scan(&row)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%#v\n", row)
	return db, nil
}
