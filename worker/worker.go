package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func worker(id int, jobs <-chan int, wg *sync.WaitGroup) {
	defer wg.Done()
	for j := range jobs {
		// fmt.Println("worker", id, "started  job", j)
		rand.Seed(time.Now().UnixNano())
		// n := rand.Intn(10) // n will be between 0 and 10
		time.Sleep(time.Duration(1) * time.Second)
		fmt.Println("worker ", id, "-", j)
		// results <- j * 2

	}

}

var maxWorker = 2000
var jobNumber = 10000

func main() {
	a := time.Now()
	jobs := make(chan int, 100)
	// results := make(chan int, 10)
	wg := new(sync.WaitGroup)

	for w := 1; w <= maxWorker; w++ {
		// fmt.Println("create worker ", w)
		go worker(w, jobs, wg)
		wg.Add(1)
	}

	fmt.Println("start assign work job")
	time.Sleep(time.Second)
	for j := 1; j <= jobNumber; j++ {
		jobs <- j
	}
	close(jobs)

	//for a := 1; a <= 5; a++ {
	//	<-results
	//}

	wg.Wait()
	b := time.Now()
	c := b.Sub(a)
	fmt.Println(c)
}
