package main

import (
	"fmt"
	"reflect"
)

type Employee struct {
	SumOfCash       float64
	SumOfCredit     float64
	SumOfBankAmount float64
	Age             int
	Job             string
	SumOfItemAmount float32
	TaxAmount       float32
}

func getFieldString(e interface{}, field string) string {
	r := reflect.ValueOf(e)
	f := reflect.Indirect(r).FieldByName(field)
	return f.String()
}

func getFeildName(e interface{}, i int) string {
	el := reflect.ValueOf(e).Elem()
	y := el.Type().Field(i).Name
	// t := el.Type().Field(i).Type

	return y
}
func getFeildType(e interface{}, i int) string {
	el := reflect.ValueOf(e).Elem()
	y := el.Type().Field(i).Type.String()
	// t := el.Type().Field(i).Type

	return y
}

func getFieldInteger(e interface{}, field string) int {
	r := reflect.ValueOf(e)
	f := reflect.Indirect(r).FieldByName(field)

	return int(f.Int())
}

func getFieldFloat(e interface{}, field string) float32 {
	r := reflect.ValueOf(e)
	f := reflect.Indirect(r).FieldByName(field)

	return float32(f.Float())
}
func main() {
	e := Employee{100.00, 99.00, 98.00, 30, "ST", 10.00, 10.00}
	long := reflect.TypeOf(e).NumField()
	fmt.Println(long)
	for i := 0; i < long; i++ {
		fmt.Println(getFeildName(&e, i))
		a := getFeildName(&e, i)
		fmt.Println(getFeildType(&e, i))

		switch getFeildType(&e, i) {
		case "float64":
			fmt.Println(getFieldFloat(&e, a))
		case "float32":
			fmt.Println(getFieldFloat(&e, a))
		case "int":
			fmt.Println(getFieldInteger(&e, a))
		case "string":
			fmt.Println(getFieldString(&e, a))
		default:
			fmt.Printf("%s.\n", getFeildType(&e, i))
		}
	}

}
