module jwt

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/go-redis/redis v6.15.8+incompatible // indirect
	github.com/go-redis/redis/v7 v7.4.0 // indirect
	github.com/twinj/uuid v1.0.0 // indirect
)
