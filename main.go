package main

import (
	"fmt"
	"log"
	"os/exec"
	"runtime"
	"time"

	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"github.com/denisbrodbeck/machineid"
	//"gitlab.com/satit13/sandbox/machine"
)

type authObj struct {
	VendingUid string `json:"vending_uuid,omitempty"`
}

type tokenObj struct {
	Token string `json:"token",omitempty`
}

type url struct {
	Result string `json:"result"`
	Data   struct {
		ShopTitle   string `json:"shop_title"`
		LogoUrl     string `json:"logo_url"`
		ShopWebsite string `json:"shop_website"`
		HomeUrl     string `json:"home_url"`
	}
}

const (
	urlTest       string = "https://test.paybox.work"
	urlProduction string = "https://cloud.paybox.work"
	urlLocalhost  string = "http://localhost:8080"
)

func main() {
	// test format datetime
	testDateTimeString2time()
	// open chrome
	openChrome()
}
func openChrome(){
	fmt.Println("os : ", runtime.GOOS)

	// get token from test.paybox.work  --> send by vending_uuid
	strUid := "41888c74fe3d4b30b83b9a702bef10c4"

	// get machine id to auth with cloud.paybox.work
	strUid, _ = machineid.ID()
	//fmt.Println("machine_id : ", strUid)


	auth := authObj{VendingUid: strUid}
	resp, _ := auth.Send(urlProduction, "/device/auth")
	//resp, _ := auth.Send("http://localhost:8080", "/device/auth")

	s := resp
	fmt.Println("string = ", s)
	a := tokenObj{
		Token: "",
	}
	err := json.Unmarshal([]byte(s), &a)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("a struct value : ", a.Token)

	// get home_url
	urlStr, err := a.GetConfig(urlProduction, "/device/config")
	if err != nil {
		return
	}
	fmt.Println("string url  = ", urlStr)
	u := url{}
	err = json.Unmarshal([]byte(urlStr), &u)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("url struct value : ", u.Data.HomeUrl)
	Open(u.Data.HomeUrl)

	for {
		fmt.Println("chrome session ")
		time.Sleep(1 * time.Minute)
	}

}
func (tk *tokenObj) GetConfig(ApiServerURL, endpointUrl string) (result string, err error) {

	url := ApiServerURL + endpointUrl

	//fmt.Println("method *bcar.send() to Endpoint ", url)
	// Send host uuid with sale post data.
	xAccessToken := tk.Token

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(tk)
	fmt.Println("payload : ", b)

	req, err := http.NewRequest("POST", url, b)
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	req.Header.Add("X-Access-Token", xAccessToken)
	resp, err := http.DefaultClient.Do(req)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Response Body:", string(body))
	//fmt.Println("X-Access-Token : ", xAccessToken)
	//fmt.Println("Api service URL : ", ApiServerURL)
	fmt.Println("endpoint : ", url)

	if err != nil {
		log.Println(err)
		return "", err
	}
	if resp.StatusCode != 200 {
		fmt.Println("Response Status:", resp.Status)
		fmt.Println("Response Headers:", resp.Header)
		//body, _ := ioutil.ReadAll(resp.Body)
		//fmt.Println("Response Body:", string(body))
		return "", errors.New(string(body))
	}

	//fmt.Println("Post ลูกค้าใหม่ Cloud -> customer.send()")
	return string(body), nil
}

func (auth *authObj) Send(ApiServerURL, endpointUrl string) (result string, err error) {

	url := ApiServerURL + endpointUrl

	//fmt.Println("method *bcar.send() to Endpoint ", url)
	// Send host uuid with sale post data.
	//xAccessToken := token

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(auth)
	fmt.Println("payload : ", b)

	req, err := http.NewRequest("POST", url, b)
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	//req.Header.Add("X-Access-Token", xAccessToken)
	resp, err := http.DefaultClient.Do(req)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Response Body:", string(body))
	//fmt.Println("X-Access-Token : ", xAccessToken)
	//fmt.Println("Api service URL : ", ApiServerURL)
	fmt.Println("endpoint : ", url)

	if err != nil {
		log.Println(err)
		return "", err
	}
	if resp.StatusCode != 200 {
		fmt.Println("Response Status:", resp.Status)
		fmt.Println("Response Headers:", resp.Header)
		//body, _ := ioutil.ReadAll(resp.Body)
		//fmt.Println("Response Body:", string(body))
		return "", errors.New(string(body))
	}

	//fmt.Println("Post ลูกค้าใหม่ Cloud -> customer.send()")
	return string(body), nil
}

func config(tk string) string {
	fmt.Println("config receive token ", tk)

	return "https://cnn.com"
}
func blockForever() {
	Open("https://www.nopadol.com")
	select {}
}
func testDateTimeString2time() {
	input := "30/06/2019 22:15:22"
	layout := "02/01/2006 15:04:05"
	t, _ := time.Parse(layout, input)
	fmt.Println(t)                               // 2017-08-31 00:00:00 +0000 UTC
	fmt.Println(t.Format("02/01/2006 15:04:05")) // 31-Aug-2017
}

func openbrowser(url string) {
	var err error

	exec.Command("killall chrome").Start()
	exec.Command("killall chromium-browser").Start()

	fmt.Println("os : ", runtime.GOOS)
	switch runtime.GOOS {
	case "linux":
		//err = exec.Command("xdg-open", url ).Start()
		exec.Command("google-chrome", url, " --kiosk").Start()
		exec.Command("/usr/bin/chromium-browser", url, " --kiosk").Start()

		//cmd = exec.Command("/usr/bin/chromium-browser","https://ui.paybox.work","--kiosk")

	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url, "--fullscreen").Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		log.Fatal(err)
	}
}

func openBrowser1(url string) bool {
	var args []string
	switch runtime.GOOS {
	case "darwin":
		args = []string{"open"}
	case "windows":
		args = []string{"cmd", "/c", "start"}
	default:
		args = []string{"xdg-open", "--headless", "--disable-extensions", "--kiosk"}
	}
	cmd := exec.Command(args[0], append(args[1:], url)...)
	return cmd.Start() == nil
}

func exportOsHOMEURL() {
	os.Setenv("home_url", "https://www.cnn.com")
}

// Copyright 2016 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package browser provides utilities for interacting with users' browsers.

// Commands returns a list of possible commands to use to open a url.
func Commands() [][]string {
	var cmds [][]string
	if exe := os.Getenv("BROWSER"); exe != "" {
		cmds = append(cmds, []string{exe})
	}
	switch runtime.GOOS {
	case "darwin":
		cmds = append(cmds, []string{"/usr/bin/open"})
	case "windows":
		cmds = append(cmds, []string{"cmd", "/c", "start"})
	default:
		if os.Getenv("DISPLAY") != "" {
			// xdg-open is only for use in a desktop environment.
			fmt.Println("start default case ")
			//cmds = append(cmds, []string{"xdg-open","--kiosk"})
			//cmds = append(cmds, []string{"xdg-open"})

		}
	}
	cmds = append(cmds,
		[]string{"chrome"},
		[]string{"google-chrome"},
		[]string{"chromium-browser"},
		[]string{"chromium"},
		[]string{"firefox"},
	)

	fmt.Println(cmds)
	return cmds
}

// Open tries to open url in a browser and reports whether it succeeded.
func Open(url string) bool {
	CloseChrome()
	for _, args := range Commands() {
		cmd := exec.Command(args[0], append(args[1:], url, " --kiosk")...)
		fmt.Println("cmd.Args = ", cmd.Args)
		if cmd.Start() == nil && appearsSuccessful(cmd, 3*time.Second) {
			//if cmd.Start() == nil {
			fmt.Println("start finish")
			return true
		}
	}
	return false

}

// appearsSuccessful reports whether the command appears to have run successfully.
// If the command runs longer than the timeout, it's deemed successful.
// If the command runs within the timeout, it's deemed successful if it exited cleanly.
func appearsSuccessful(cmd *exec.Cmd, timeout time.Duration) bool {
	errc := make(chan error, 1)
	go func() {
		errc <- cmd.Wait()
	}()

	select {
	case <-time.After(timeout):
		return true
	case err := <-errc:
		return err == nil
	}
}

func CloseChrome() {
	cmd := exec.Command("pkill chromium")
	cmd.Start()

	cmd = exec.Command("killall chrome")
	cmd.Start()

	cmd = exec.Command("killall google-chrome")
	cmd.Start()

}
