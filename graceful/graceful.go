package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/denisenkom/go-mssqldb"
	"github.com/jmoiron/sqlx"
)

func main() {

	// Create connection database
	db, err := ConnectDemo()

	if err != nil {
		fmt.Println(err)
	}

	router := http.NewServeMux()

	// Router
	router.HandleFunc("/readiness", func(res http.ResponseWriter, req *http.Request) {

		rows, err := db.Query("select distinct docdate from bcarinvoice where year(docdate)=2020 and month(docdate)=2")
		if err != nil {
			fmt.Println(err)
			return
		}
		defer rows.Close()

		var sysDate string
		for rows.Next() {
			rows.Scan(&sysDate)
			fmt.Fprintln(res, "Readiness... ", sysDate)
		}

		fmt.Fprintln(res, "Readiness... ", sysDate)
	})

	server := &http.Server{
		Handler: router,
		Addr:    fmt.Sprintf(":%s", "8080"),
	}

	go func() {
		fmt.Println(server.ListenAndServe())
	}()

	// Gracefully Shutdown
	// Make channel listen for signals from OS
	gracefulStop := make(chan os.Signal, 1)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)

	<-gracefulStop

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		fmt.Printf("Error shutting down server %s", err)
	} else {
		fmt.Println("Server gracefully stopped")
	}

	if err := db.Close(); err != nil {
		fmt.Printf("Error closing db connection %s", err)
	} else {
		fmt.Println("DB connection gracefully closed")
	}
}

func ConnectDemo() (msdb *sqlx.DB, err error) {
	db_host := "192.168.0.7"
	db_name := "bcnp"
	db_user := "sa"
	db_pass := "[ibdkifu"

	port := "1433"
	dsn := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%s;database=%s", db_host, db_user, db_pass, port, db_name)
	msdb = sqlx.MustConnect("mssql", dsn)
	if msdb.Ping() != nil {
		fmt.Println("Error ")
	}
	return msdb, nil
}
