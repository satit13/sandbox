package main

import (
	"fmt"
	"io"
	"os"
	"strings"

	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

func main() {
	var r io.Reader = strings.NewReader("text in the single-byte encoding- การบ้าน")
	fmt.Println("before transform")
	io.Copy(os.Stdout, r)
	r = transform.NewReader(r, charmap.Windows874.NewDecoder())

	io.Copy(os.Stdout, r)
	fmt.Println("\n")
}
