package main

import (
	"context"
	"fmt"
	"log"
	"sync"

	pubsub "cloud.google.com/go/pubsub"
	"google.golang.org/api/option"
)

var tpn = "rice"

func main() {
	ctx := context.Background()
	// option.WithCredentialsFile("./pubsub.json")
	projectID := "pordee-project"
	// option.clientOpt()
	client, err := pubsub.NewClient(ctx, projectID, option.WithCredentialsFile("./pordee-project-pubsub.json"))

	if err != nil {
		log.Printf("Error when creating pubsub client. Err: %v", err)
		return
	} else {
		fmt.Println("yes...")
	}

	// topic, err := pc.CreateTopic(context.Background(), "projects/credit9-io/topics/test")
	// tp := pubsub.SubscriptionConfig{}
	// tp.Topic = topic
	// sub, err := pc.CreateSubscription(context.Background(), "sub-test-1", tp)
	// sub := pc.Subscription("test_view_msg")

	// // sub.ReceiveSettings.MaxExtension = 30 * time.Second
	// err = sub.Receive(context.Background(), func(ctx context.Context, m *pubsub.Message) {
	// 	log.Printf("Got message: %s", m.Data)
	// 	m.Ack()
	// })
	// if err != nil {
	// 	// Handle error.
	// }
	// publish
	ExampleTopic_Publish(client)

	var mu sync.Mutex
	received := 0
	sub := client.Subscription("pos")

	cctx, cancel := context.WithCancel(ctx)
	err = sub.Receive(cctx, func(ctx context.Context, msg *pubsub.Message) {
		fmt.Printf("Got message: %q\n", string(msg.Data))
		msg.Ack()
		mu.Lock()
		defer mu.Unlock()
		received++
		if received == 10 {
			cancel()
		}
	})
	// if err != nil {
	// 	return
	// }
	return

	// return &pubSubClient{psclient: client}, nil
}

func ExampleTopic_Publish(client *pubsub.Client) {
	ctx := context.Background()
	// client, err := pubsub.NewClient(ctx, "project-id")
	// if err != nil {
	// 	// TODO: Handle error.
	// }

	topic := client.Topic("pos")
	defer topic.Stop()
	// var results []*pubsub.PublishResult
	topic.Publish(ctx, &pubsub.Message{
		Data: []byte("hello world"),
	})
	return
}
