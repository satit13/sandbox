package main

import (
	"fmt"
	"github.com/go-ldap/ldap"
	"log"
)

type AdUser struct {
	User string
	Name string
}

func main() {

	// tlsConfig := &tls.Config{InsecureSkipVerify: true}
	// l, err := ldap.DialTLS("tcp", "ldap.example.com:636", tlsConfig)

	// No TLS, not recommended
	l, err := ldap.Dial("tcp", "192.168.0.10:389")

	err = l.Bind("backup@nopadol.com", "[ibdki")
	if err != nil {
		// error in ldap bind
		log.Println(err)
	}
	// successful bind
	fmt.Println("bind success ")
	users, _ := search(l, "satit")
	fmt.Println("completed ", users)

}

func search(l *ldap.Conn, username string) (ret []AdUser, err error) {
	// err := l.Bind("backup@nopadol.com", "[ibdki")
	// if err != nil {
	// 	log.Printf("failed to bind:%v", err)
	// 	return "", err
	// }

	// userLists := []AdUser{}

	searchRequest := ldap.NewSearchRequest(
		"OU=IT,OU=BD,OU=Nopadol,DC=nopadol,DC=com",
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		// fmt.Sprintf("(&(sAMAccountName=%s)(objectclass=user))", username),
		fmt.Sprintf("(&(objectclass=user))"),
		[]string{"dn", "cn", "sAMAccountName"},
		nil,
	)
	sr, err := l.Search(searchRequest)

	if err != nil {
		log.Printf("failed to search:%v", err)
		return ret, err
	}

	for _, entry := range sr.Entries {
		// fmt.Printf("%s: %v\n", entry.DN, entry.GetAttributeValue("sAMAccountName"))
		fmt.Printf(" %v\n", entry.GetAttributeValue("sAMAccountName"))

		adu := AdUser{}
		adu.User = entry.GetAttributeValue("sAMAccountName")
		adu.Name = entry.GetAttributeValue("cn")
		ret = append(ret, adu)
	}

	// if len(sr.Entries) != 1 {
	// 	log.Printf("no such user or too many")
	// 	return "", fmt.Errorf("User not find or too many. count=%d", len(sr.Entries))
	// }
	userdn := sr.Entries[0].DN
	log.Printf("user dn found: %s", userdn)
	return
}
